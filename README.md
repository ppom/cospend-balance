# cospend-balance

This program permits to copy all bills from Nextcloud Cospend projects into another.

## Rationale

I'm using one Cospend project to note my expenses.
One member of this project represents the expenses, the other represents the incomes.

I'm also using Cospend projects to share expenses with housemates & close friends.

I wanted a view of all this projects merged into a new one, representing the sum of my expenses 📊

## Usage

See my [settings.yml](./settings.yml) for a full, detailed usage.

You'll need [Go](https://go.dev/).

Edit your settings file, then run `go run . /path/to/your/settings.yml` to launch the program.

You also can run `go build .` then move the freshly created `cospend-balance` executable
somewhere in your `PATH`, for example to `/usr/local/bin/`.
You should be able to use it like this: `cospend-balance /path/to/your/settings.yml`

## Speed

My laptop (client) and server are on the same local network.

Two tests:
- first test: it's the first time the project is synced. All 900 present bills are deleted and replaced by 900 new ones
- second test: it's not the first time the project is synced. Only 5 changed, deleted and created bills are updated.

### First test

The total real time transferring the bills takes ~1 min to complete.

- ~01 s user+sys time consumed on the client 
- ~50 s user+sys time consumed on the phpfpm-nextcloud server
- ~25 s user+sys time consumed on the postgresql server

### Second test

The total real time transferring the bills takes ~2 s to complete.

- ~0.6 s user+sys time consumed on the client 
- ~0.2 s user+sys time consumed on the phpfpm-nextcloud server
- ~0.2 s user+sys time consumed on the postgresql server

## Contributions

… are welcome. If you want to be sure it'll be accepted, file an issue first.

Bug reports, feature requests and questions are welcome too. File an issue!

But I doubt someone on Earth has the same use-case 🤡

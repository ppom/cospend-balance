package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"sync"
)

func main() {
	yes := flag.Bool("y", false, "do not prompt for confirmation before syncing")
	flag.Parse()
	settings := flag.Arg(0)
	if settings == "" {
		fmt.Println("cospend-balance requires exactly one argument, which is the configuration file")
		fmt.Println("(you can additionnaly add the -y flag to suppress the confirmation prompt)")
		os.Exit(1)
	}

	conf := readConf(settings)

	// Async call
	var confirmation chan bool
	if !*yes {
		confirmation = BoolAsk(fmt.Sprintf("Do you really want to sync project %v (url %v) from the other projects?\n"+
			"Any bill not coming from them will be deleted.", conf.MainProject.Name, conf.MainProject.Url))
	}

	nc, err := NewNextcloudConnection(conf.baseUrl)
	if err != nil {
		fmt.Println("Fatal error while creating connection to server:\n", err)
		os.Exit(2)
	}

	if !*yes && !(<-confirmation) {
		fmt.Println("Aborting.")
		os.Exit(0)
	}

	var currentBillsSet map[int]Bill
	var wantedBillsSet map[int]Bill

	finishedFetchingMainBills := make(chan bool)
	finishedFetchingOtherBills := make(chan bool)
	finishedCUDoperations := make(chan bool)

	// Define pool of workers that will do Create-Update-Delete operations
	CUDoperations := make(chan func() error)
	CUDf := func() {
		for op := range CUDoperations {
			err = op()
			if err != nil {
				fmt.Printf("ERROR: %v\n", err)
			}
		}
		finishedCUDoperations <- true
	}
	go CUDf()
	go CUDf()

	// Read all bills already in the main project
	go func() {
		mainBills, err := nc.FetchBills(conf.MainProject.shareId)
		if err != nil {
			fmt.Printf("Fatal error while reading bills from project %v: %v\n", conf.MainProject.Name, err)
			os.Exit(2)
		}
		deletions := make(chan int)
		currentBillsSet = mainBillsAsMap(mainBills, deletions)
		for id := range deletions {
			CUDoperations <- func() error { return nc.DeleteBill(conf.MainProject.shareId, id) }
		}
		finishedFetchingMainBills <- true
	}()

	// Make one channel for all bills from different projects
	// It will be closed when all projects are done sending bills
	bills := make(chan Bill)
	var wg sync.WaitGroup
	wg.Add(len(conf.OtherProjects))
	go func() {
		wg.Wait()
		close(bills)
	}()

	// We create a set of the desired new state
	go func() {
		wantedBillsSet = otherBillsAsMap(bills)
		finishedFetchingOtherBills <- true
	}()

	fmt.Println("Fetching bills from all projects...")

	// Each project send its bills
	for _, project := range conf.OtherProjects {
		projectBills, err := nc.FetchBills(project.shareId)
		if err != nil {
			fmt.Printf("Fatal error while fetching bills of project %v: %v\n", project.Name, err)
			os.Exit(2)
		}
		go func(bs chan Bill, project Project) {
			for b := range bs {
				takeBill, bill := b.FromProject(conf, project)
				if takeBill {
					bills <- bill
				}
			}
			wg.Done()
		}(projectBills, project)
	}

	// TODO We compare the two sets, send CUD operations & close its channel
	<-finishedFetchingMainBills
	<-finishedFetchingOtherBills

	fmt.Println("Comparing current and desired states...")

	for id, currentBill := range currentBillsSet {
		_, ok := wantedBillsSet[id]
		if !ok {
			fmt.Printf("Deleting bill %v\n", currentBill.What)
			CUDoperations <- func() error { return nc.DeleteBill(conf.MainProject.shareId, currentBill.Id) }
		}
	}
	for id, wantedBill := range wantedBillsSet {
		currentBill, ok := currentBillsSet[id]
		if !ok {
			fmt.Printf("Creating bill %v\n", wantedBill.What)
			CUDoperations <- func() error { return nc.NewBill(conf.MainProject.shareId, wantedBill.BillPost()) }
		} else {
			wantedBill.Id = currentBill.Id
			if !reflect.DeepEqual(currentBill, wantedBill) {
				fmt.Printf("Updating bill %v\n", wantedBill.What)
				CUDoperations <- func() error { return nc.UpdateBill(conf.MainProject.shareId, wantedBill) }
			}
		}

	}
	close(CUDoperations)

	// Wait for Create-Update-Delete operations to complete
	<-finishedCUDoperations
	<-finishedCUDoperations

	// We're done!
	fmt.Printf("Done\n")
	os.Exit(0)
}

func SyncAsk(prompt string) string {
	var str string
	r := bufio.NewReader(os.Stdin)
	for {
		fmt.Fprint(os.Stderr, prompt+" ")
		str, _ = r.ReadString('\n')
		if str != "" {
			break
		}
	}
	return strings.TrimSpace(str)
}

func Ask(prompt string) chan string {
	response := make(chan string)
	go func() {
		response <- SyncAsk(prompt)
		close(response)
	}()
	return response
}

func BoolAsk(prompt string) chan bool {
	response := make(chan bool)
	go func() {
		s := SyncAsk(fmt.Sprintf("%v (y/n):", prompt))
		response <- s == "y" || s == "Y"
		close(response)
	}()
	return response
}

// Compute a channel of Bills into a
// map[<id of the original bill in an "other project">]<BillPost that (should) represent it on the main project>
func mainBillsAsMap(billChan <-chan Bill, deletions chan<- int) map[int]Bill {
	billsSet := make(map[int]Bill)
	go func() {
		idRegex := regexp.MustCompile("^\\[(\\d+)\\]")
		for b := range billChan {
			billIdStrings := idRegex.FindStringSubmatch(b.Comment)
			if len(billIdStrings) == 2 && len(billIdStrings[1]) > 0 {
				billId, err := strconv.Atoi(billIdStrings[1])
				if err != nil {
					// Bill doesn't contain a bill Id as first line of comment
					fmt.Println("WARN: bill in main project lacks an original bill id, deleting.")
					fmt.Println(b)
					deletions <- b.Id
				} else if _, present := billsSet[billId]; present {
					// Bill is a duplicate
					fmt.Printf("WARN: bill in main project is duplicate (id %v), deleting.\n", billId)
					fmt.Println(b)
					fmt.Println(billsSet[billId])
					deletions <- b.Id
				} else {
					billsSet[billId] = b
				}
			} else {
				// Bill doesn't contain a bill Id as first line of comment
				fmt.Println("WARN: bill in main project lacks an original bill id, deleting.")
				fmt.Println(b)
				deletions <- b.Id
			}
		}
		close(deletions)
	}()
	return billsSet
}

func otherBillsAsMap(billChan <-chan Bill) map[int]Bill {
	billsSet := make(map[int]Bill)
	for b := range billChan {
		if _, present := billsSet[b.Id]; present {
			fmt.Println("ERROR: There are duplicate ids in the other projects. This should not happen.")
		} else {
			billsSet[b.Id] = b
		}
	}
	return billsSet
}

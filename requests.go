package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/cookiejar"
)

type NextcloudConnection struct {
	csrftoken string
	baseUrl   string
	client    http.Client
}

func NewNextcloudConnection(baseUrl string) (NextcloudConnection, error) {
	var nc NextcloudConnection
	nc.baseUrl = baseUrl
	var err error
	nc.client.Jar, err = cookiejar.New(nil)
	if err != nil {
		return nc, err
	}
	err = nc.fetchCookiesAndCSRFToken()
	return nc, err
}

func (nc *NextcloudConnection) fetchCookiesAndCSRFToken() error {
	req, err := nc.client.Get(fmt.Sprintf("%s/csrftoken", nc.baseUrl))
	if err != nil {
		return err
	}
	if req.StatusCode != 200 {
		return fmt.Errorf("Got code %v while retrieving CSRF token", req.StatusCode)
	}
	defer req.Body.Close()
	var t Token
	err = json.NewDecoder(req.Body).Decode(&t)
	nc.csrftoken = t.Token
	return err
}

func (nc *NextcloudConnection) addToken(req *http.Request) {
	req.Header.Add("requesttoken", nc.csrftoken)
}

func (nc *NextcloudConnection) FetchBills(projectId string) (chan Bill, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%v/apps/cospend/apiv3/projects/%v/nopass/bills", nc.baseUrl, projectId), nil)
	if err != nil {
		return nil, err
	}
	nc.addToken(req)
	res, err := nc.client.Do(req)
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("Got %v while fetching bills", res.StatusCode)
	}
	bills := make(chan Bill)
	go func() {
		defer res.Body.Close()
		defer close(bills)
		dec := json.NewDecoder(res.Body)

		// Pass the first tokens
		for i := 0; i < 5; i++ {
			_, err := dec.Token()
			if err != nil {
				log.Fatal(err)
			}
		}

		// Get the bills
		for dec.More() {
			var b Bill
			err := dec.Decode(&b)
			if err != nil {
				log.Fatal(err)
			}
			bills <- b
		}
	}()
	return bills, nil
}

func (nc *NextcloudConnection) DeleteBills(projectId string) error {
	bills, err := nc.FetchBills(projectId)
	if err != nil {
		return err
	}
	errors := make(chan error)
	done := make(chan bool)
	const ROUTINES = 2
	for i := 0; i < ROUTINES; i++ {
		go func() {
			for bill := range bills {
				err := nc.DeleteBill(projectId, bill.Id)
				if err != nil {
					errors <- err
					break
				}
			}
			done <- true
		}()
	}
	counter := 0
	for counter < ROUTINES {
		select {
		case err := <-errors:
			return err
		case <-done:
			counter++
		}
	}
	return nil
}

func (nc *NextcloudConnection) DeleteBill(projectId string, billId int) error {
	req, err := http.NewRequest("DELETE", fmt.Sprintf("%v/apps/cospend/api/projects/%v/nopass/bills/%v", nc.baseUrl, projectId, billId), nil)
	if err != nil {
		return err
	}
	nc.addToken(req)
	res, err := nc.client.Do(req)
	if res.StatusCode != 200 {
		return fmt.Errorf("Got %v while deleting bill", res.StatusCode)
	}
	return nil
}

func (nc *NextcloudConnection) NewBill(projectId string, bp BillPost) error {
	jsonbytes, err := json.Marshal(bp)
	if err != nil {
		return err
	}
	req, err := http.NewRequest("POST", fmt.Sprintf("%v/apps/cospend/api/projects/%v/nopass/bills", nc.baseUrl, projectId), bytes.NewReader(jsonbytes))
	if err != nil {
		return err
	}
	nc.addToken(req)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	res, err := nc.client.Do(req)
	if res.StatusCode != 200 {
		body, _ := io.ReadAll(res.Body)
		return fmt.Errorf("Got %v while creating bill: %v", res.StatusCode, string(body))
	}
	var billId int
	err = json.NewDecoder(res.Body).Decode(&billId)
	if err != nil {
		return err
	}
	return nil
}

// TODO check the real API & adapt
func (nc *NextcloudConnection) UpdateBill(projectId string, bill Bill) error {
	jsonbytes, err := json.Marshal(bill.BillPost())
	if err != nil {
		return err
	}
	req, err := http.NewRequest("PUT", fmt.Sprintf("%v/apps/cospend/api/projects/%v/nopass/bills/%v", nc.baseUrl, projectId, bill.Id), bytes.NewReader(jsonbytes))
	if err != nil {
		return err
	}
	nc.addToken(req)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	res, err := nc.client.Do(req)
	if res.StatusCode != 200 {
		body, _ := io.ReadAll(res.Body)
		return fmt.Errorf("Got %v while updating bill: %v", res.StatusCode, string(body))
	}
	return nil
}

// func main() {
// 	conf := readConf("./settings.yml")

// 	nc, err := NewNextcloudConnection("https://file.ppom.me")
// 	if err != nil {
// 		log.Fatalln(err)
// 	}
// 	bills, err := nc.FetchBills(conf.MainProject.shareId)
// 	if err != nil {
// 		log.Fatalln(err)
// 	}
// 	for bill := range bills {
// 		fmt.Printf("%v\n", bill)
// 	}
// 	bill, err := nc.NewBill(conf.MainProject.shareId, BillPost{
// 		Amount:        150,
// 		What:          "Pain semoule",
// 		Payer:         conf.MainProject.Payer,
// 		Payed_for:     conf.MainProject.PayedFor,
// 		Comment:       "",
// 		Timestamp:     Timestamp{time.Now()},
// 		Paymentmodeid: 0,
// 		Categoryid:    0,
// 		Repeat:        "n",
// 	})
// 	if err != nil {
// 		log.Fatalln("error creating bill:", err)
// 	}
// 	fmt.Println("New bill:", bill)
// }

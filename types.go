package main

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// GET example.com/apps/cospend/projects/example/bills
type Bills []Bill

type Bill struct {
	Id            int       `json:"id"`
	Amount        Euro      `json:"amount"`
	What          string    `json:"what"`
	Comment       string    `json:"comment"`
	Timestamp     Timestamp `json:"timestamp"`
	Payer_id      int       `json:"payer_id"`
	OwerIds       []int     `json:"owerIds"`
	Paymentmodeid int       `json:"paymentmodeid"`
	Categoryid    int       `json:"categoryid"`
}

// POST example.com/apps/cospend/projects/example/bills
type BillPost struct {
	Amount        Euro      `json:"amount"`
	What          string    `json:"what"`
	Comment       string    `json:"comment"`
	Timestamp     Timestamp `json:"timestamp"`
	Payer         int       `json:"payer"`
	Payed_for     Payers    `json:"payed_for"`
	Paymentmodeid int       `json:"paymentmodeid"`
	Categoryid    int       `json:"categoryid"`
	Repeat        string    `json:"repeat"`
}

type Euro int
type Payers []int
type Timestamp struct{ t time.Time }

func (e Euro) MarshalJSON() ([]byte, error) {
	return []byte(e.String()), nil
}

var onlyDecimalRegexp = regexp.MustCompile("\\.[0-9]$") // ex: 0.1, 4.5

func (e *Euro) UnmarshalJSON(b []byte) error {
	coef := 100
	s := strings.ReplaceAll(string(b), "\"", "")
	s = strings.ReplaceAll(s, " ", "")

	if strings.Contains(s, ".") {
		if onlyDecimalRegexp.MatchString(s) { // ex: 0.1
			coef = 10
		} else {
			coef = 1
		}
		s = strings.Replace(s, ".", "", 1)
	}
	value, err := strconv.ParseInt(s, 10, 32)
	*e = Euro(int(value) * coef)
	return err
}

func (e Euro) String() string {
	return fmt.Sprintf("%v.%02v", int(e)/100, int(e)%100)
}

func (p Payers) MarshalJSON() ([]byte, error) {
	if len(p) == 0 {
		return json.Marshal("")
	}
	s := strconv.Itoa(p[0])
	for i := 1; i < len(p); i++ {
		s = fmt.Sprint(s, ",", strconv.Itoa(p[1]))
	}
	return json.Marshal(s)
}

func (t Timestamp) MarshalJSON() ([]byte, error) {
	s := t.t.Unix()
	return json.Marshal(s)
}

func (t *Timestamp) UnmarshalJSON(b []byte) error {
	i, err := strconv.ParseInt(string(b), 10, 64)
	t.t = time.Unix(i, 0)
	return err
}

func (t Timestamp) String() string {
	return t.t.String()
}

func (bp BillPost) Bill(id int) Bill {
	return Bill{
		id,
		bp.Amount,
		bp.What, bp.Comment,
		bp.Timestamp,
		bp.Payer,
		bp.Payed_for,
		bp.Paymentmodeid, bp.Categoryid,
	}
}

func (b Bill) BillPost() BillPost {
	return BillPost{
		b.Amount,
		b.What, b.Comment,
		b.Timestamp,
		b.Payer_id,
		b.OwerIds,
		b.Paymentmodeid, b.Categoryid,
		"n",
	}
}

func (b Bill) FromProject(conf Conf, project Project) (bool, Bill) {
	if _, present := conf.shadowedMembers[b.Payer_id]; present {
		return false, b
	}

	b.What = fmt.Sprintf("(%v) %v", project.Name, b.What)
	b.Comment = fmt.Sprintf("[%v]\n%v", b.Id, b.Comment)

	realOwers := 0
	for _, o := range b.OwerIds {
		if _, present := conf.shadowedMembers[o]; !present {
			realOwers++
		}
	}
	if realOwers == 0 {
		return false, b
	}
	b.Amount = Euro(int(b.Amount) / realOwers)

	if project.Ower == b.Payer_id {
		b.Paymentmodeid = conf.paymentModes[b.Paymentmodeid]
	} else {
		b.Paymentmodeid = conf.otherpayedpaymentmode
	}
	b.Categoryid = conf.categories[b.Categoryid]

	b.Payer_id = conf.MainProject.Payer
	b.OwerIds = conf.MainProject.PayedFor

	return true, b
}

// GET nextcloud.example.com/csrftoken
type Token struct {
	Token string
}

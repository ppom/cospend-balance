package main

import (
	"fmt"
	"log"
	"net/url"
	"os"
	"strings"

	"gopkg.in/yaml.v3"
)

type Conf struct {
	MainProject   MainProject
	OtherProjects []Project

	categories, paymentModes map[int]int
	shadowedMembers          map[int]bool
	otherpayedpaymentmode    int

	baseUrl string
}

type MainProject struct {
	Name, Id string
	Payer    int
	PayedFor []int

	Otherpayedpaymentmode                string
	Categoriesbyslug, Paymentmodesbyslug map[string]int

	Urlfile, Url string
	shareId      string
}

type Project struct {
	Name, Id        string
	Ower            int
	ShadowedMembers []int

	Categoriesbyid, Paymentmodesbyid map[int]string

	Urlfile, Url string
	shareId      string
}

func readFileOrDie(filename string) []byte {
	data, err := os.ReadFile(filename)
	if err != nil {
		log.Fatalf("FATAL Failed to read file %v: %v", filename, err)
	}
	return data
}

func readConf(filename string) Conf {

	data := readFileOrDie(filename)

	var conf Conf
	err := yaml.Unmarshal(data, &conf)
	if err != nil {
		log.Fatalln("FATAL Failed to parse configuration file:", err)
	}

	conf.categories = make(map[int]int)
	conf.paymentModes = make(map[int]int)
	conf.shadowedMembers = make(map[int]bool)

	// Categories and Payment Modes are nullable
	// When null, 0 is given
	conf.categories[0] = 0
	conf.paymentModes[0] = 0

	var ok bool

	// Category used when bill has been payed by another person
	otherpayedpaymentmodeId, ok := conf.MainProject.Paymentmodesbyslug[conf.MainProject.Otherpayedpaymentmode]
	if !ok {
		log.Fatalf("mainproject.otherpayedpaymentmode (%v) is not present in mainproject.paymentmodesbyslug", conf.MainProject.Otherpayedpaymentmode)
	}
	conf.otherpayedpaymentmode = otherpayedpaymentmodeId

	// URL
	if conf.MainProject.Url == "" {
		conf.MainProject.Url = strings.TrimSpace(string(readFileOrDie(conf.MainProject.Urlfile)))
	}

	castedMainUrl, err := url.Parse(conf.MainProject.Url)
	if err != nil {
		log.Fatalf("Main project url '%v' is malformed", conf.MainProject.Url)
	}
	conf.MainProject.shareId = castedMainUrl.Path[strings.LastIndex(castedMainUrl.Path, "/s/")+3:]

	conf.baseUrl = fmt.Sprintf("%v://%v", castedMainUrl.Scheme, castedMainUrl.Host)

	for i, project := range conf.OtherProjects {

		// URL

		if conf.OtherProjects[i].Url == "" {
			conf.OtherProjects[i].Url = strings.TrimSpace(string(readFileOrDie(conf.OtherProjects[i].Urlfile)))
		}

		castedUrl, err := url.Parse(conf.OtherProjects[i].Url)
		if err != nil {
			log.Fatalf("Main project url '%v' is malformed", conf.MainProject.Url)
		}

		if castedMainUrl.Scheme != castedUrl.Scheme || castedMainUrl.Host != castedUrl.Host {
			log.Fatalf("Project %v has a different protocol or domain than the main project. Synchronising accross different servers is currently not supported.", project.Name)
		}

		conf.OtherProjects[i].shareId = castedUrl.Path[strings.LastIndex(castedUrl.Path, "/s/")+3:]

		// Categories

		for id, category := range project.Categoriesbyid {
			conf.categories[id], ok = conf.MainProject.Categoriesbyslug[category]
			if !ok {
				log.Fatalf("Project '%v' category '%v' is not present in main project", project.Name, category)
			}
		}

		// Payment modes

		for id, paymentmode := range project.Paymentmodesbyid {
			conf.paymentModes[id], ok = conf.MainProject.Paymentmodesbyslug[paymentmode]
			if !ok {
				log.Fatalf("Project '%v' payment mode '%v' is not present in main project", project.Name, paymentmode)
			}
		}

		// Payer exclusion
		for _, sm := range project.ShadowedMembers {
			conf.shadowedMembers[sm] = true
		}
	}
	return conf
}

// func main() {
// 	conf := readConf("./settings.yml")
// 	fmt.Printf("%#v\n", conf)
// }

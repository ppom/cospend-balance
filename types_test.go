package main

import (
	"fmt"
	"reflect"
	"testing"
)

func TestEuroMarshaling(t *testing.T) {
	tests := []struct {
		ramValue  Euro
		jsonValue []byte
	}{
		{Euro(0), []byte("0.00")},
		{Euro(1), []byte("0.01")},
		{Euro(2), []byte("0.02")},
		{Euro(10), []byte("0.10")},
		{Euro(11), []byte("0.11")},
		{Euro(100), []byte("1.00")},
		{Euro(101), []byte("1.01")},
		{Euro(110), []byte("1.10")},
		{Euro(111), []byte("1.11")},
	}

	unmarshalOnlyTests := []struct {
		ramValue  Euro
		jsonValue []byte
	}{
		{Euro(10), []byte("0.1")},
		{Euro(110), []byte("1.1")},
		{Euro(100), []byte("1.0")},
	}

	for _, tt := range tests {
		testName := fmt.Sprintf("Marshal %v → %v", int(tt.ramValue), string(tt.jsonValue))
		t.Run(testName, func(t *testing.T) {
			res, err := tt.ramValue.MarshalJSON()
			if err != nil {
				t.Errorf("Error while marshaling %v: %v", int(tt.ramValue), err)
			} else if !reflect.DeepEqual(res, tt.jsonValue) {
				t.Errorf("Euro %v should be marshaled as '%v', not '%v'", int(tt.ramValue), string(tt.jsonValue), string(res))
			}
		})
	}

	for _, tt := range append(tests, unmarshalOnlyTests...) {
		testName := fmt.Sprintf("Unmarshal %v → %v", string(tt.jsonValue), int(tt.ramValue))
		t.Run(testName, func(t *testing.T) {
			var e Euro
			err := e.UnmarshalJSON(tt.jsonValue)
			if err != nil {
				t.Errorf("Error while unmarshaling %v: %v", string(tt.jsonValue), err)
			} else if !reflect.DeepEqual(e, tt.ramValue) {
				t.Errorf("string '%v' should be marshaled as %v, not %v", string(tt.jsonValue), int(tt.ramValue), int(e))
			}
		})
	}
}
